package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {

        Scanner intFactorial = new Scanner(System.in);

        try{
            System.out.println("Input an integer whose factorial will be computed(WHILE LOOP)");

            int num = intFactorial.nextInt();
            if (num <= 0) {
                // this gets caught in the catch block
                throw new IllegalArgumentException();
            }

            int factorial = 1;

            int num2 = num;

            while(num > 1){
                factorial *= num;
                num--;
            }
            System.out.println("The factorial of " + num2 + " is " + factorial);

            System.out.println("Input an integer whose factorial will be computed(FOR LOOP)");
            int num3 = intFactorial.nextInt();

            if (num3 <= 0) {
                // this gets caught in the catch block
                throw new IllegalArgumentException();
            }

            int i = 0;
            int factorial2 = 1;
            for(i = 1; i <= num3; i++){
                factorial2 *= i;
            }
            System.out.println("The factorial of " + num3 + " is " + factorial2);


        } catch (InputMismatchException e){
            System.out.println("Input is not a number");
        } catch(Exception e){
            System.out.println("0 and Negative Numbers are not allowed");
        }


}}